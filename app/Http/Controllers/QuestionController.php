<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;


class QuestionController extends Controller
{
    public function create()
    {
        return view('layouts.create');
    }

    public function store(Request $request)
    {
        // cara pertama
        $request->validate([
            'judul' => 'required|unique:question',
            'isi' => 'required',
        ]);
        $query = DB::table('question')->insert([
            "title" => $request["judul"],
            "body" => $request["isi"]
        ]);
        return redirect('/question');
    }

    public function index()
    {
        $question = DB::table('question')->get();
        return view('layouts.index', compact('question'));
    }

    public function shows($id)
    {
        $question = Question::where('id', $id)->first();
        return view('layouts.shows', compact('question'));
    }

    public function edit($id) 
    {
        $question = Question::where('id',$id)->first();
        return view('layouts.edit', compact('question'));
    }

    public function update(Request $request, $id)
    {
        $question = Question::where('id', $id)->update([
            'title' => $request->judul,
            'body' => $request->isi
        ]);
        return redirect('/question');
    }

    public function delete($id)
    {
        Product::destroy($id);
        return back();
    }
}
