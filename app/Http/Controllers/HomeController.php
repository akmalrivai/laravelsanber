<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home() 
    {
        return view('items.main');
    }
    
    public function show() 
    {
        return view('items.show');
    }
}
