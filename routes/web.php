<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', 'HomeController@home');

// Route::get('/show', 'HomeController@show');

// Route::get('/register', 'AuthController@register');

// Route::get('/welcome', 'AuthController@welcome');

Route::get('/question', 'QuestionController@index');

Route::get('/question/create', 'QuestionController@create');

Route::post('/question', 'QuestionController@store');

Route::get('/question/show/{pertanyaan_id}', 'QuestionController@shows');

Route::get('question/edit/{pertanyaan_id}', 'QuestionController@edit');

Route::put('question/update/{pertanyaan_id}', 'QuestionController@update');

Route::delete('/delete/{pertanyaan_id}', 'QuestionController@delete');