<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Baru</title>
</head>
<body>
    <div class="container">
        <h1>Buat Akun Baru!</h1>
        <h2> Sign Up Form</h2>
    </div>
    <div class="container" style="width: 35%; margin-bottom: 10px;">
        <form action="/welcome">
            @csrf
            <div class="form-group" style="margin-top: 5px;">
                <label for="">First Name :</label><br><br>
                <input type="text" class="form-control"><br><br>
            </div>
            <div class="form-group">
                <label for="">Last Name :</label><br><br>
                <input type="text" class="form-control"><br><br>
            </div>
            <br>
            <label for="">Gender</label> <br><br>
           <input type="radio" name="gender" value="L">Male <br>
           <input type="radio" name="gender" value="P">Female <br>
           <input type="radio" name="gender" value="O">Other <br>
           <br>

           <label for="">Nationality</label><br><br>
           <select name="nation" id="nation">
               <option value="Indonesian">Indonesian</option>
               <option value="Thailand">Thailand</option>
               <option value="Malaysian">Malaysian</option>
               <option value="Singapore">Singapore</option>
               <option value="Phillipines">Phillipines</option>
               <option value="Myanmar">Myanmar</option>
               <option value="Cambodja">Cambodja</option>
               <option value="Brunei">Brunei</option>
           </select>
           
           <br><br>
           
           <label for="">Bio :</label><br><br>
           <textarea name="Bio" id="bio" cols="40" rows="10"></textarea>
           <br>
           <button type="submit" id="btn-submit" class="btn btn-primary mt-3">Sign up</button>
        </form>
    </div>
    


</body>
</html>