@extends('template.master')

@section('content')
<h2>Show Question {{$question->id}}</h2>
<h4>{{$question->title}}</h4>
<p>{{$question->body}}</p>
@endsection