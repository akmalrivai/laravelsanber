<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnswerLikeDislikeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer_like_dislike', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('point');
            $table->biginteger('profil_id')->unsigned();
            $table->foreign('profil_id')->references('id')->on('profil');
            $table->biginteger('jawaban_id')->unsigned();
            $table->foreign('jawaban_id')->references('id')->on('answer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answer_like_dislike');
    }
}
