<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionLikeDislikeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_like_dislike', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('point');
            $table->biginteger('profil_id')->unsigned();
            $table->foreign('profil_id')->references('id')->on('profil');
            $table->biginteger('pertanyaan_id')->unsigned();
            $table->foreign('pertanyaan_id')->references('id')->on('question');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_like_dislike');
    }
}
